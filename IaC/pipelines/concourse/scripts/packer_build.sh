#! /usr/bin/env sh

set -e -x

packer --version

packer build -force peteches-tf-code/IaC/build/packer/peteches-vm.json

