#! /usr/bin/env sh

set -e -x

terraform --version

cd peteches-tf-code/IaC/deploy/terraform

terraform init
terraform plan -out plan.txt
terraform apply plan.txt
