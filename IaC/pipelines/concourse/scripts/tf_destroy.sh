#! /usr/bin/env sh

set -e -x

terraform --version

cd peteches-tf-code/IaC/deploy/terraform

terraform init
terraform destroy -force
